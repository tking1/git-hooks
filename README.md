# Sentiment Hook

Adds git hook to your current project to ensure that commit messages meet specified requirements such as sentiment, passive voice, profanities, readability, repeated words, and simplification. 

### Prerequisites

Node.js, npm, and git are required to run this. The project must also be setup of with npm and git. If it is not, run the following commands:

```
npm init
```
```
git init
```

### Installing

Installation is simple. Just run:

```
npm install https://gitlab.com/tking1/git-hooks.git
```

and fill out the form in the installation wizard.

## Authors

* **Thomas King** - *Initial work* - [InRhythm](https://gitlab.com/InRhythm)

## Acknowledgments

* Sandro
* unified
* retext

