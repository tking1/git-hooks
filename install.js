const fs = require('fs');
const blessed = require('blessed');

// initialize the screen object
const screen = blessed.screen({
  smartCSR: true,
  keys: true
});

// set the title in the terminal
screen.title = 'Sentiment Install';

// we should exit the program on <esc>, <q>, or <ctrl>+<c>
screen.key(['escape', 'q', 'C-c'], function(a, b) {
  return process.exit(0);
});

// create a form for entering the data
const form = blessed.form({
  parent: screen,
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
  keys: true
});

// label for the sentiment threshold
const oCheck = blessed.checkbox({
  parent: form,
  content: 'Overuse: ',
  checked: true,
  top: 1,
  left: 1,
  height: 1,
  width: 16
});

// label for ignore on overuse
const oiLabel = blessed.text({
  parent: screen,
  content: 'Ignore: ',
  left: 18,
  top: 1,
  height: 1,
  width: 10
});

// text input for overuse ignore
const oIgnore = blessed.textbox({
  parent: form,
  inputOnFocus: true,
  top: 0,
  height: 3,
  left: 29,
  width: '100%-31',
  border: {
    type: 'line'
  },
  style: {
    focus: {
      inverse: true
    }
  }
});

// label for overuse limit
const olLabel = blessed.text({
  parent: screen,
  content: 'Limit: ',
  left: 18,
  top: 4,
  height: 1,
  width: 10,
});

// text input for overuse limit 
const oLimit = blessed.textbox({
  parent: form,
  inputOnFocus: true,
  top: 3,
  height: 3,
  left: 29,
  width: 5,
  border: {
    type: 'line'
  },
  style: {
    focus: {
      inverse: true
    }
  }
});

// label for passive
const pCheck = blessed.checkbox({
  parent: form,
  content: 'Passive: ',
  checked: true,
  left: 1,
  top: 7,
  height: 1,
  width: 16
});

const prCheck = blessed.checkbox({
  parent: form,
  content: 'Profanities: ',
  checked: true,
  left: 1,
  top: 10
});

const priLabel = blessed.text({
  parent: screen,
  content: 'Ignore: ',
  left: 18,
  top: 10
});

const prIgnore = blessed.textbox({
  parent: form,
  inputOnFocus: true,
  top: 9,
  height: 3,
  left: 29,
  width: '100%-31',
  border: {
    type: 'line'
  },
  style: {
    focus: {
      inverse: true
    }
  }
});

const prsLabel = blessed.text({
  parent: screen,
  content: 'Sureness: ',
  left: 18,
  top: 13
});

const prSureness = blessed.textbox({
  parent: form,
  inputOnFocus: true,
  top: 12,
  height: 3,
  left: 29,
  width: 5,
  border: {
    type: 'line'
  },
  style: {
    focus: {
      inverse: true
    }
  }
});

const rCheck = blessed.checkbox({
  parent: form,
  content: 'Readability: ',
  checked: true,
  left: 1,
  top: 16
});

const rtLabel = blessed.text({
  parent: screen,
  content: 'Threshold: ',
  left: 18,
  top: 16
});

const rThreshold = blessed.textbox({
  parent: form,
  inputOnFocus: true,
  top: 15,
  height: 3,
  left: 29,
  width: 5,
  border: {
    type: 'line'
  },
  style: {
    focus: {
      inverse: true
    }
  }
});

const reCheck = blessed.checkbox({
  parent: form,
  content: 'Repeat-words: ',
  checked: true,
  left: 1,
  top: 19,
  height: 1,
});

const sCheck = blessed.checkbox({
  parent: form,
  content: 'Sentiment: ',
  checked: true,
  left: 1,
  top: 22
});

const stLabel = blessed.text({
  parent: screen,
  content: 'Threshold: ',
  left: 18,
  top: 22
});

const sThreshold = blessed.textbox({
  parent: form,
  inputOnFocus: true,
  top: 21,
  height: 3,
  left: 29,
  width: 5,
  border: {
    type: 'line'
  },
  style: {
    focus: {
      inverse: true
    }
  }
});

const siCheck = blessed.checkbox({
  parent: form,
  content: 'Simplify: ',
  checked: true,
  top: 25,
  left: 1,
  height: 1,
  width: 16
});

// label for ignore on overuse
const siiLabel = blessed.text({
  parent: screen,
  content: 'Ignore: ',
  left: 18,
  top: 25,
  height: 1,
  width: 10
});

// text input for overuse ignore
const siIgnore = blessed.textbox({
  parent: form,
  inputOnFocus: true,
  top: 24,
  height: 3,
  left: 29,
  width: '100%-30',
  border: {
    type: 'line'
  },
  style: {
    focus: {
      inverse: true
    }
  }
});

// button for submitting the form
const submit = blessed.button({
  parent: form,
  content: 'Submit',
  top: 27,
  left: 5,
  width: '100%-10',
  height: 3,
  align: 'center',
  padding: {
    top: 1
  },
  style: {
    focus: {
      inverse: true
    }
  }
});

// the submit button will submit the form
submit.on('press', function() {
  form.submit();
});

function toList(str) {
  return str.split(',').map(x => x.trim());
}

// what to do on submit
form.on('submit', function(data) {
  // create output config file
  const output = {};
  if (oCheck.checked) {
    output.overuse = { enabled: true };
    if (oIgnore.getValue()) {
      output.overuse.ignore = toList(oIgnore.getValue());
    }
    if (oLimit.getValue()) {
      output.overuse.limit = parseInt(oLimit.getValue());
    }
  }
  if (pCheck.checked) {
    output.passive = { enabled: true };
  }
  if (prCheck.checked) {
    output.profanities = { enabled: true };
    if (prIgnore.getValue()) {
      output.profanities.ignore = toList(prIgnore.getValue());
    }
    if (prSureness.getValue()) {
      output.profanities.sureness = parseInt(prSureness.getValue());
    }
  }
  if (rCheck.checked) {
    output.readability = { enabled: true };
    if (rThreshold.getValue()) {
      output.readability.threshold = parseInt(rThreshold.getValue());
    }
  }
  if (reCheck.checked) {
    output['repeated-words'] = { enabled: true };
  }
  if (sCheck.checked) {
    output.sentiment = { enabled: true };
    if (sThreshold.getValue()) {
      output.sentiment.threshold = parseInt(sThreshold.getValue());
    }
  }
  if (siCheck.checked) {
    output.simplify = { enabled: true };
    if (siIgnore.getValue()) {
      output.simplify.ignore = toList(siIgnore.getValue());
    }
  }

  // write the config file to the project's root directory
  fs.writeFileSync('../../retextConfig.json', JSON.stringify(output, null, 2));
  // create a .huskyrc file to run the processor on `commit-msg` hook
  fs.writeFileSync('../../.huskyrc', JSON.stringify({hooks: {"commit-msg": "exec < /dev/tty && node node_modules/sentiment-hook/process.js"}}, null, 2));
  // exit the script
  process.exit(0);
});

// render the screen
screen.render();
