const unified = require('unified');
const retext = require('retext');
const english = require('retext-english');
const sentiment = require('retext-sentiment');
const nlcstToString = require('nlcst-to-string');
const report = require('vfile-reporter');

// list of available retext plugins
const AVAILABLE_PLUGINS = [
  'overuse',
  'passive',
  'profanities',
  'readability',
  'repeated-words',
  'simplify'
];

// used to colorize the tree text
const red = '{#ff0000-fg}{bold}';
const green = '{#00ff00-fg}{bold}';
const close = '{/}';

// utility function for updating the tree to include values
function colorizeTree(node) {
  // only add a color if this word has a polarity and value
  if (node.data && node.data.polarity && node.value) {
    let color = '';
    // bad words are red
    if (node.data.polarity < 0) {
      color = red;
    }
    // good words are green
    else if (node.data.polarity > 0) {
      color = green;
    }
    if (color) {
      node.value = `${color}${node.value}${close}`;
    }
  }
  // colorize all the children of this node as well
  if (node.children) {
    for (let i in node.children) {
      colorizeTree(node.children[i]);
    }
  }
}

// class for processing messages
class Processor {
  constructor(config) {
    // set up the processor for sentiment
    if (config.sentiment && config.sentiment.enabled) {
      console.log('adding sentiment');
      this.processor = unified().use(english).use(sentiment);
      this.sentiment = config.sentiment.threshold || 0;
    }
    // set up the basic retext processor for reporting
    this.retextProcessor = retext().use(english);


    // check for which plugins should be enabled in the reporting
    for (let i in AVAILABLE_PLUGINS) {
      if (config[AVAILABLE_PLUGINS[i]] && config[AVAILABLE_PLUGINS[i]].enabled) {
        console.log(`adding ${AVAILABLE_PLUGINS[i]}`);
        this.retextProcessor = this.retextProcessor.use(require(`retext-${AVAILABLE_PLUGINS[i]}`), config[AVAILABLE_PLUGINS[i]]);
      }
    }

    // give default values to neccessary variables
    this.message = '';
    this.report = '';
    this.tree = {};
  }

  // analayze the given message and store the results internally
  analyze(message) {
    // make sure this is a new message to save processing the same message twice
    if (message == this.message) return;
    this.message = message;

    // check the sentiment and colorize the tree
    if (this.processor) {
      this.tree = this.processor.parse(message);
      this.processor.run(this.tree, message);
      colorizeTree(this.tree);
    }

    // create the report message from the retext processing
    this.report = report(this.retextProcessor.processSync(message));
  }

  // return true iff the tests pass (no warning in retext and the sentiment meets the threshold
  get passesTests() {
    if (this.processor) {
      return this.report == 'no issues found' && this.tree.data.polarity >= this.sentiment;
    }
    return this.report == 'no issues found';
  }

  // returns a string that is colorized according to the sentiment of each word
  get colorized() {
    if (this.processor) {
      return nlcstToString(this.tree);
    }
    return '';
  }
}

module.exports = Processor;
