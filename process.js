#!/usr/bin/env node

const vfile = require('to-vfile');
const fs = require('fs');

const Processor = require('./processor');

// get the location of the commit message, either from the environment variable when used as a hook or the program arguments when testing locally
const commit = process.env.HUSKY_GIT_PARAMS || process.argv[2];
// load the config file
const config = JSON.parse(vfile.readSync('retextConfig.json'));
const processor = new Processor(config);

// load the commit file
const file = vfile.readSync(commit);
let contents = file.toString('utf8');
contents = contents.split('\n').filter(line => !line.startsWith('#')).join('\n');
console.log(contents);

processor.analyze(contents);

// if the message passes initial tests, we can proceed
if (processor.passesTests) {
  process.exit(0);
}
else {
  console.log('Failed tests');
}

// when the message does not pass the tests, we will create the blessed interface
const blessed = require('blessed');

// create a screen object
const screen = blessed.screen({
  smartCSR: true,
  keys: true
});

// set the terminal title
screen.title = 'Commit Sentiment';

// we should exit with a failure if <esc>, <q>, or <ctrl>+<c> is pressed
screen.key(['escape', 'q', 'C-c'], function(a, b) {
  return process.exit(1);
});

// create the form for editing the commit message
const form = blessed.form({
  parent: screen,
  top: 0,
  left: 'center',
  width: '100%',
  height: '50%',
  keys: true
});

// textarea for editing the commit message
const text = blessed.textarea({
  parent: form,
  top: 0,
  left: 0,
  width: '100%',
  height: '100%-2',
//  inputOnFocus: true,
  border: {
    type: 'line'
  },
  style: {
    fg: 'white',
    border: {
      fg: '#f0f0f0'
    },
    focus: {
      bg: 'black',
      fg: 'white'
    }
  },
  keys: true,
  vi: true
});

// button to submit the form
const submit = blessed.button({
  parent: form,
  name: 'submit',
  content: 'Submit',
  top: '100%-2',
  left: 1,
  height: 2,
  width: '40%-2',
  align: 'center',
  style: {
    focus: {
      inverse: true
    }
  }
});

// button to run the analysis
const analyze = blessed.button({
  parent: form,
  name: 'analyze',
  content: 'Analyze',
  top: '100%-2',
  left: '40%',
  height: 2,
  width: '40%-2',
  align: 'center',
  style: {
    focus: {
      inverse: true
    }
  }
});

// button to exit the script without changing the message, will not proceed with the commit
const cancel = blessed.button({
  parent: form,
  name: 'cancel',
  content: 'Cancel',
  top: '100%-2',
  left: '80%+1',
  height: 2,
  width: '20%-2',
  align: 'center',
  style: {
    focus: {
      inverse: true
    }
  }
});

// box that will contain the analysis
const analysis = blessed.box({
  parent: screen,
  top: '50%',
  left: 0,
  height: '50%-1',
  width: '100%'
});

let sent = false;
let polarity = false;

if (config.sentiment && config.sentiment.enabled) {
  // displays the current sentiment of the message and the target
  polarity = blessed.text({
    parent: analysis,
    content: `Target: ${processor.sentiment} | Current: ${processor.tree.data.polarity}`,
    top: 0,
    left: 0,
    width: '40%',
    height: '1'
  });

  // displays a colorized version of the message to indicate sentiment of words
  sent = blessed.text({
    parent: analysis,
    content: processor.colorized,
    tags: true,
    top: 1,
    left: 0,
    width: '40%',
    height: '100%-1',
    alwaysScroll: true,
    scrollable: true,
    keys: true,
    vi: true
  });
}

// shows the warning message for the retext processor
const warnings = blessed.text({
  parent: analysis,
  content: processor.report,
  tags: true,
  top: 0,
  left: (config.sentiment && config.sentiment.enabled) ? '40%' : 0,
  width: (config.sentiment && config.sentiment.enabled) ? '60%': '100%',
  height: '100%',
  alwaysScroll: true,
  scrollable: true,
  keys: true,
  vi: true
});
  
// submit the form on press
submit.on('press', function() {
  form.submit();
});
  
// exit when cancel is pressed
cancel.on('press', function() {
  process.exit(1);
});

// update the analysis text
const updateMessages = function() {
  msg.display('analyzing');
  if (config.sentiment && config.sentiment.enabled) {
    sent.setContent('.');
  }
  warnings.setContent('.');
  screen.render();
  let value = text.getValue();
  processor.analyze(value);
  if (config.sentiment && config.sentiment.enabled) {
    polarity.setContent(`Target: ${processor.sentiment} | Current: ${processor.tree.data.polarity}`);
    sent.setContent(processor.colorized);
  }
  warnings.setContent(processor.report);
  msg.display('completed analysis');
  screen.render();
};

// run the analysis
analyze.on('press', updateMessages);

// create a message item for display information to the user
const msg = blessed.message({
  parent: screen,
  top: '100%-1',
  left: 'center',
  height: 1,
  width: '100%'
});

// set the default value to the current commit message
text.setValue(processor.message);

// we should update the analysis when the user moves focus from the commit message
text.on('blur', updateMessages);

// submitting the form
form.on('submit', function() {
  // check to see if the current message will pass the tests
  let value = text.getValue();
  processor.analyze(value);
  if (processor.passesTests) {
    // once the message passes tests, we should write it as the new commit message and exit the script
    fs.writeFileSync(commit, value);
    process.exit(0);
  }
  else {
    // let the user know that there are still issues and the message cannot be submitted
    msg.display('Please verify that your message meets the requirements.');
  }
});

// display the screen
screen.render();

// set up autoscroll for the warning message
const autoscroll = function() {
  // if we are at the bottom, start from the top again
  if (warnings.getScrollPerc() >= 100) {
    warnings.setScrollPerc(0);
  }
  // scroll down one line
  else {
    let scrollIndex = warnings.getScroll();
    scrollIndex += 1;
    warnings.scrollTo(scrollIndex);
  }
  // rerender the screen
  screen.render();
};

// set up the auto scroll to scroll once per second
setInterval(autoscroll, 1000);
